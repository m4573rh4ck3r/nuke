// +build linux

package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
)

var (
	includeDescriptions bool
)

func init() {
	rootCmd.AddCommand(completionCmd)

	cobra.OnInitialize()

	completionCmd.Flags().BoolVar(&includeDescriptions, "include-descriptions", false, "include command descriptions in completion output")
}

var completionCmd = &cobra.Command{
	Use:       "completion",
	Short:     "print out shell completion code",
	Args:      cobra.ExactArgs(1),
	Example:   "nuke completion bash",
	ValidArgs: []string{"bash", "zsh", "fish"},
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			log.Fatalf("%s completion requires exactly one argument. Received %d\n", rootCmd.Use, len(args))
		}
		switch args[0] {
		case "bash":
			rootCmd.GenBashCompletion(os.Stdout)
		case "zsh":
			rootCmd.GenZshCompletion(os.Stdout)
		case "fish":
			rootCmd.GenFishCompletion(os.Stdout, true)
		default:
			log.Fatalf("%s: invalid argument\n", args[0])
		}
	},
}
