package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/m4573rh4ck3r/nuke/internal/version"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "show detailed version output",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		log.New(os.Stdout, "", log.Lmsgprefix).Printf(version.DetailedVersionTemplate, rootCmd.Use, version.Version, version.GitRevision, version.GitBranch, version.GoVersion, version.BuildTime, version.OsArch)
	},
}
