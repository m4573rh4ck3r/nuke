// +build linux

package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/m4573rh4ck3r/nuke/pkg/nuke"
)

var (
	iterations    int
	deleteAll     bool
	zero          bool
	followSymlink bool
	force         bool
	recursive     bool
)

func init() {
	rootCmd.Flags().IntVarP(&iterations, "iterations", "n", 3, "overwrite n times")
	rootCmd.Flags().BoolVarP(&zero, "zero", "z", false, "do a final overwrite with zeros before deallocating file space to hide shredding")
	rootCmd.Flags().BoolVarP(&deleteAll, "delete", "d", false, "delete files and directories afterwards")
	rootCmd.Flags().BoolVarP(&followSymlink, "follow-symlink", "s", true, "follow symlinks")
	rootCmd.Flags().BoolVarP(&force, "force", "f", false, "change file permissions to allow writing if necessary")
	rootCmd.Flags().BoolVarP(&recursive, "recursive", "r", false, "recurse over directories and subdirectories")
}

var rootCmd = &cobra.Command{
	Use:     "nuke",
	Short:   "overwrite a given file with random data",
	Example: fmt.Sprintf("%s filenames...", os.Args[0]),
	Args:    cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		for _, filename := range args {
			if err := nuke.NukeFile(filename, iterations, deleteAll, zero, followSymlink, force, recursive); err != nil {
				log.SetOutput(os.Stderr)
				log.Fatal(err)
			}
		}
	},
}

func Execute() {
	rootCmd.Execute()
}
