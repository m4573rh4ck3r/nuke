package progress

import (
	"testing"
	"time"
)

func TestRun(t *testing.T) {
	bar := NewBar(200, "=")
	bar.Init()
	for i := 1; i <= 200; i++ {
		bar.Run(int64(i))
		time.Sleep(time.Millisecond * 50)
	}
	bar.Finish()
}
