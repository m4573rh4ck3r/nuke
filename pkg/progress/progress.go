package progress

import (
	"fmt"
	"strconv"
)

type Bar struct {
	Percent      int64
	Current      int64
	Total        int64
	Rate         string
	ProgressChar string
}

func Format(n int64) string {
	in := strconv.FormatInt(n, 10)
	numOfDigits := len(in)
	if n < 0 {
		numOfDigits-- // First character is the - sign (not a digit)
	}
	numOfCommas := (numOfDigits - 1) / 3

	out := make([]byte, len(in)+numOfCommas)
	if n < 0 {
		in, out[0] = in[1:], '-'
	}

	for i, j, k := len(in)-1, len(out)-1, 0; ; i, j = i-1, j-1 {
		out[j] = in[i]
		if i == 0 {
			return string(out)
		}
		if k++; k == 3 {
			j, k = j-1, 0
			out[j] = ','
		}
	}
}

func NewBar(total int64, progressChar string) *Bar {
	bar := &Bar{
		Total:        total,
		ProgressChar: progressChar,
	}
	return bar
}

func (b *Bar) Init() {
	for i := 0; i < int(b.Percent); i++ {
		b.Rate += b.ProgressChar
	}
}

func (b *Bar) GetPercent() int64 {
	return int64(float32(b.Current) / float32(b.Total) * 100)
}

func (b *Bar) Run(current int64) {
	b.Current = current
	b.Percent = b.GetPercent()

	var rate string
	for i := 0; i < int(b.GetPercent()); i++ {
		rate += "="
	}
	rate += ">"
	for i := 0; i < 100-int(b.GetPercent()); i++ {
		rate += " "
	}

	fmt.Printf("\r[%s] %s %s/%s", rate, fmt.Sprintf("%d%s", b.GetPercent(), "%"), Format(b.Current), Format(b.Total))
}

func (b *Bar) Finish() {
	fmt.Println()
}
