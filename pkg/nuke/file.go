// +build linux

package nuke

import (
	"crypto/rand"
	"fmt"
	"log"
	"math"
	"os"
	"path/filepath"
	"sort"

	"golang.org/x/sys/unix"

	"gitlab.com/m4573rh4ck3r/nuke/pkg/progress"
)

func NukeFile(filename string, iterations int, deleteAll bool, zero bool, followSymlink bool, force bool, recursive bool) error {
	var files []*os.File
	var dirs []string

	fileInfo, err := os.Lstat(filename)
	if err != nil {
		log.Fatalln(err)
	}

	if fileInfo.Mode()&os.ModeSymlink == os.ModeSymlink {
		// filename is a symlink
		symfile, err := os.OpenFile(filename, os.O_RDWR, 0666)
		if err != nil {
			log.Fatalln(err)
		}
		files = append(files, symfile)
		defer symfile.Close()

		if followSymlink {
			originalFilename, err := os.Readlink(filename)
			if err != nil {
				log.Fatalln(err)
			}
			file, err := os.OpenFile(originalFilename, os.O_RDWR, 0666)
			if err != nil {
				log.Fatalln(err)
			}
			defer file.Close()
			files = append(files, file)
		}

	} else if fileInfo.IsDir() {
		if !recursive {
			log.Fatalln("cannot recurse over directories without -r flag set")
		}
		// recursively add all files in directory
		if err := filepath.WalkDir(filename, func(path string, d os.DirEntry, err error) error {
			fi, err := os.Lstat(path)
			if err != nil {
				return err
			}
			abs, err := filepath.Abs(path)
			if err != nil {
				return err
			}
			if fi.IsDir() {
				dirs = append(dirs, path)
			} else {
				f, err := os.OpenFile(abs, os.O_RDWR, 0666)
				if err != nil {
					return err
				}
				files = append(files, f)
			}
			return nil
		}); err != nil {
			log.Fatalln(err)
		}

	} else {
		// filename is a regular file
		file, err := os.OpenFile(filename, os.O_RDWR, 0666)
		if err != nil {
			log.Fatalln(err)
		}
		defer file.Close()
		files = append(files, file)
	}

	for _, file := range files {
		// get exclusive lock on file to prevent other applications from writing to it
		if err := unix.Flock(int(file.Fd()), unix.LOCK_EX); err != nil {
			log.Fatalln(err)
		}

		var fileSize int64 = fileInfo.Size()
		const fileChunk = 1

		os.Truncate(filename, fileInfo.Size()+10)
		file.Seek(0, 0)

		totalPartsNum := uint64(math.Ceil(float64(fileSize) / float64(fileChunk)))

		lastPosition := 0

		// increase total iterations by one if the final overwrite will be with zeros
		if zero {
			iterations++
		}

		// overwrite file
		for x := 0; x < iterations; x++ {
			log.Printf("iteration %d of %d\n", x+1, iterations)

			if zero && x+1 == iterations {
				// this is the final iteration and the zeros flag was set, so overwrite file with zeros
				log.Printf("overwriting file %s with zeros\n", file.Name())
				overwriteBar := progress.NewBar(int64(totalPartsNum), "=")
				overwriteBar.Init()
				for i := uint64(0); i < totalPartsNum; i++ {

					partSize := int(math.Min(fileChunk, float64(fileSize-int64(i*fileChunk))))

					_, err = file.WriteAt([]byte("0"), int64(lastPosition))

					if err != nil {
						log.Fatalln(err)
					}
					overwriteBar.Run(int64(i + 1))

					lastPosition = lastPosition + partSize
				}
				overwriteBar.Finish()
				lastPosition = 0
			} else {
				// overwrite file with random data
				overwriteBar := progress.NewBar(int64(totalPartsNum), "=")
				overwriteBar.Init()
				log.Printf("overwriting file %s with random data\n", file.Name())
				for i := uint64(0); i < totalPartsNum; i++ {
					partSize := int(math.Min(fileChunk, float64(fileSize-int64(i*fileChunk))))

					b := make([]byte, 1)
					if _, err := rand.Read(b); err != nil {
						log.Fatalln(err)
					}
					_, err = file.WriteAt(b, int64(lastPosition))

					if err != nil {
						log.Fatalln(err)
					}
					overwriteBar.Run(int64(i + 1))

					lastPosition = lastPosition + partSize
				}
				overwriteBar.Finish()
				lastPosition = 0
			}
		}

		if deleteAll {
			fmt.Println("")
			log.Printf("dealocating file %s\n", file.Name())
			if err := unix.Fallocate(int(file.Fd()), unix.FALLOC_FL_PUNCH_HOLE|unix.FALLOC_FL_KEEP_SIZE, 0, fileSize); err != nil {
				log.Fatalln(err)
			}
			log.Printf("removing file %s\n", file.Name())
			if err := os.Remove(file.Name()); err != nil {
				return err
			}
		}
	}

	if deleteAll && recursive {
		revDirs := sort.Reverse(sort.StringSlice(dirs))
		sort.Sort(revDirs)
		for _, d := range dirs {
			if err := os.Remove(d); err != nil {
				log.Fatalln(err)
			}
			log.Printf("successfully removed directory %s\n", d)
		}
	}
	return nil
}
