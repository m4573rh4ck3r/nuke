// +build linux

package main

import (
	"log"

	"gitlab.com/m4573rh4ck3r/nuke/internal/cmd"
)

func main() {
	log.SetFlags(log.Lshortfile)
	cmd.Execute()
}
